//
//  ViewController.swift
//  MVPPP
//
//  Created by Ta Tuan Macbook on 3/13/19.
//  Copyright © 2019 HiUPTeam. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var presenter: ViewControllerPresenter?
    
    @IBOutlet weak var label: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = ViewControllerPresenter(delegate: self)
//        presenter?.setViewDelegate(delegateView: self)
       
    }
    
    @IBAction func yellowColor(_ sender: Any) {
        presenter?.changeColorBackground(color: "yellow")
    }
    
    @IBAction func blueColor(_ sender: Any) {
        presenter?.changeColorBackground(color: "blue")
    }
    
    
    @IBAction func redColor(_ sender: Any) {
        presenter?.changeColorBackground(color: "red")
    }
    
}
extension ViewController: ViewControllerDelegate{
    
    func hello(string: String) {
     label.text = string
    }
    
}
