//
//  PresenterViewController.swift
//  MVPPP
//
//  Created by Ta Tuan Macbook on 3/13/19.
//  Copyright © 2019 HiUPTeam. All rights reserved.
//

import Foundation
import UIKit

protocol ViewControllerDelegate: class {
    func hello(string: String)
}

class ViewControllerPresenter{
    
    let vc = ViewController()
    
    var a:Int = 100
    
    weak var delegateViewController: ViewControllerDelegate?
    
    init(delegate: ViewControllerDelegate){
        self.delegateViewController = delegate
    }
    
    //set view controller to delegate of presenter
    //    func setViewDelegate(delegateView: ViewControllerDelegate){
    //        self.delegateViewController = delegateView
    //    }
    
    
    func changeColorBackground(color: String){
        if ( color == "yellow"){
            delegateViewController?.hello(string: color)
            vc.view.backgroundColor = UIColor.yellow
        }else if color == "blue" {
            delegateViewController?.hello(string: color)
            vc.view.backgroundColor = UIColor.blue
        }else if color == "red"{
           delegateViewController?.hello(string: color)
            vc.view.backgroundColor = UIColor.red
        }
        
    }
    
}
